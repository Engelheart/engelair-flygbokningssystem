import java.util.Scanner;


public class Traveller {

	String firstName,lastName,pnr;
	int id;
	private Scanner user_input=new Scanner(System.in);
	private Scanner user_inputInt=new Scanner(System.in);
	Route myRoute = new Route();
	Database myDatabase = new Database(); 
	Person myPerson=new Person();
	
	int menuChoice;
	
	
	
	public void runTraveller(){
			
			System.out.println("\nYou are logged in as traveller");
			
			System.out.println("\nWhat do you want to do");
			do {
				System.out.println("1. Available flights.");
				System.out.println("2. Book a ticket.");
				System.out.println("3. look at your personal bookings.");
				System.out.println("4. Return to main.");
				menuChoice = user_inputInt.nextInt();
				switch (menuChoice) {
				case 1:
					listFlights();
					break;
				case 2:
					book();
					break;
				case 3:
					viewBooking();
					break;
				
				}

			} while (!(menuChoice >= 4));
			
		}
	
	public void listFlights(){
		myDatabase.getRoute();
	}
		
	public void	book(){
		String firstName,lastName,pnr;
		System.out.println("Where do you want to go? (Select with indexnumber)");
		myDatabase.getRoute();
		menuChoice=user_inputInt.nextInt();
		myPerson.setID(menuChoice);
		
		System.out.println("\nPlease enter your first name");
		firstName=user_input.next();
		myPerson.setFirstname(firstName);
		System.out.println("\nPlease enter your last name");
		lastName=user_input.next();
		myPerson.setLastname(lastName);
		System.out.println("\nPlease enter your socialsecurity number(10 digits without dash)");
		pnr=user_input.next();
		myPerson.setPnr(pnr);
		
		//set values in database booking
		myDatabase.setvaluesDb("INSERT IGNORE INTO "+Database.db+".booking(firstname,lastname,pnr,route_id) VALUES('"+myPerson.getFirstname()+"','"+myPerson.getLastname()+"','"+myPerson.getPnr()+"','"+myPerson.getID()+"')");
		
		
	}	
	public void viewBooking(){
		String pnr=null;
		System.out.println("\nPlease enter your socialsecurity number(10 digits without dash)");
		pnr=user_input.next();
		myPerson.setPnr(pnr);
		myDatabase.getBooking(myPerson.getPnr());
		
	}
	
}
