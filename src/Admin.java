import java.util.Scanner;


public class Admin {
	
	static Scanner user_input= new Scanner(System.in);
	static Scanner user_inputInt= new Scanner(System.in);
	Database myDatabase=new Database();
	Valid myValid= new Valid();
	Traveller myTraveller=new Traveller();
	public void runAdmin(){
		
		int menuChoice=0;
		System.out.println("You are logged in as Admin");
		
		do{
			System.out.println("\n What do you want to do?");
			System.out.println("1. Create an airport");
			System.out.println("2. Create a company");
			System.out.println("3. Create a route");
			System.out.println("4. Search flights between two destinations");
			System.out.println("5. Book a ticket");
			System.out.println("6. Show all airports");
			System.out.println("7. Show all companies");
			System.out.println("8. Show all bookings");
			System.out.println("9. Book a ticket");
			menuChoice=user_inputInt.nextInt();
			switch(menuChoice){
				
				case 1:
					createAirport();
					break;
				case 2:
					createCompany();
					break;
				case 3:
					createRoute();
					break;
				case 4:
					searchBetween();
					break;
				case 5:
					book();
					break;
				case 6:
					getAirport();
					break;
				case 7:
					getCompany();
					break;
				case 8:
					myDatabase.getAllBooking();
					break;
				case 9:
					myTraveller.book();
			}
		}while (!(menuChoice>=8));
			
		}	
	
	
	public void createAirport(){
		String airP= null;
		String airpName=null;
		do{
			System.out.println("Enter short name for airport (3 capitol letters)");
			airP=user_input.next();
			System.out.println("Enter full name for airport");
			airpName=user_input.next();
			if (!myValid.validAirport(airP)){
				System.out.println("Please try again (3 capitol letters");
			}
		}while (!myValid.validAirport(airP));
		Airport myAirport=new Airport(airP,airpName);
		myDatabase.setvaluesDb("INSERT IGNORE INTO " + Database.db+".airport(apt,aptname) Values ('"+myAirport.getAirp()+"','"+myAirport.getAirpName()+"')");
	}

	public void createCompany(){
		String company=null;
		
		do{
			System.out.println("Enter companyname");
			company=user_input.next();
			if(!myValid.validCompany(company)){
				System.out.println("Please try again (Starting capitol, least 6 letters)");
			}
		
		}while (!myValid.validCompany(company));
		Company myCompany=new Company(company);
		myDatabase.setvaluesDb("INSERT IGNORE INTO "+ Database.db+".company(company) Values ('"+myCompany.getCompany()+"')");
		
	}
	
	public void createRoute(){
		String query,dest,dep,date;
		int rowID;
		Route myRoute=new Route();
		dep=null;
		date=null;
		dest=null;
		query=null;
		
		
		//Selecting departure 
		myDatabase.getAirport();
		System.out.println("Please select departure (write short name in capitol letters ex OSD)");
		dep=user_input.next();
		myRoute.setDeparture(dep);
		System.out.println("You have set "+myRoute.getDeparture()+" as departure");
		System.out.println("--------------");	
		
		
		//Selecting destination
		System.out.println("\nPlease select destination (write short name in capitol letters ex. OSD)");
		myDatabase.getDest(dep);
		dest=user_input.next();
		myRoute.setDestination(dest);
		System.out.println("You have set "+myRoute.getDestination()+" as destination");
		System.out.println("--------------");
		
		//Selecting date
		System.out.println("\nPlease select a date(YYMMDD)");
		date=user_input.next();
		myRoute.setDate(date);
		System.out.println("You have set "+myRoute.getDate()+" as date");
		System.out.println("--------------");
		
		//Selecting company
		System.out.println("\nPlease select company(use ID in front of each row)");
		myDatabase.getCompany();
		rowID=user_inputInt.nextInt();
		myRoute.SetCompanyID(rowID);
		
		//insert in database
		myDatabase.setvaluesDb("INSERT IGNORE INTO "+ Database.db+".route(dep, dest, date, company_id) VALUES ('"+myRoute.getDeparture()+"','"+myRoute.getDestination()+"','"+myRoute.getDate()+"','"+myRoute.getCompanyID()+"')");
		System.out.println("Your route is now set");
		myDatabase.getRoute();
	}
	
	public void searchBetween(){
		String dep,dest;
		dep=null;
		dest=null;
		Route myRoute=new Route();
		
		//Select departure
		myDatabase.getAirport();
		System.out.println("Please select departure (write short name in capitol letters ex OSD)");
		dep=user_input.next();
		myRoute.setDeparture(dep);
		System.out.println("You have set "+myRoute.getDeparture()+" as departure");
		System.out.println("--------------");
		
		//Selecting destination
		System.out.println("\nPlease select destination (write short name in capitol letters ex. OSD)");
		myDatabase.getDest(dest);
		dest=user_input.next();
		myRoute.setDestination(dest);
		System.out.println("You have set "+myRoute.getDestination()+" as destination");
		System.out.println("--------------");
		
		//recieving possible routes between two destinations
		myDatabase.between(myRoute.getDeparture(),myRoute.getDestination());
		
	}
	
	public void book(){
		
		
	}
	
	public void getAirport(){
		myDatabase.getAirport();
	}
	
	public void getCompany(){
		myDatabase.getCompany();
	}

}
