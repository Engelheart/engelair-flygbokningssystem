
public class Route {

	private String departure;
	private String destination;
	private String date;
	private String company;
	private int companyID;
	
	public Route(){
	
	}
	
	public void setDeparture(String departure){
		this.departure=departure;
	}
	
	public String getDeparture(){
		return departure;
	}
	
	public void setDestination(String destination){
		this.destination=destination;
	}
	
	public String getDestination(){
		return destination;
	}
	
	public void setDate(String date){
		this.date=date;
	}
	
	public String getDate(){
		return date;
	}
	
	public void setCompany(String company){
		this.company=company;
	}
	
	public String getCompany(){
		return company;
	}
	
	public void SetCompanyID(int companyID){
		this.companyID=companyID;
	}
	
	public int getCompanyID(){
		return companyID;
	}
	
	
	
	
	
}
