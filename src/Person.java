
public class Person {

	String firstName,lastName,pnr;
	int id;
	public Person(){
		
	}
	public Person(String first, String last,String p, int id){
		this.firstName=first;
		this.lastName=last;
		this.pnr=p;
		this.id=id;
				
	}
	
	public void setFirstname(String first){
		this.firstName=first;
	}
	
	public void setLastname(String last){
		this.lastName=last;
	}
	
	public void setPnr(String pnr){
		this.pnr=pnr;
	}
	
	public String getFirstname(){
		return this.firstName;
	}
	
	public String getLastname(){
		return this.lastName;
	}
	
	public String getPnr(){
		return this.pnr;
	}
	public void setID(int id){
		this.id=id;
	}
	public int getID(){
		return this.id;
	}
}
