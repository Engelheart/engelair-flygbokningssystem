import java.util.regex.Pattern;


public class Valid {

		public boolean validAirport(String airP){
			
			Pattern airp = Pattern.compile("[A-Z]{3}");		
			return airp.matcher(airP).matches();
		}
		
		
		public boolean validCompany(String companyName){
			
			Pattern company = Pattern.compile("^[a-�A-�0-9]{6,}$");
			return company.matcher(companyName).matches();
		}
}
