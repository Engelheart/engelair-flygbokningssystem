import java.sql.*;

public class Database {

	
	public static String db ="engelair";
	
	private static String user = "root";
	private static String password = "Mysql2014";
	private static String url="jdbc:mysql://localhost:3306/"; 
	private static Connection conn;
	private static Statement stmt;
	
	
	public Database() {

		try {
			conn = DriverManager.getConnection(url, user, password);
			Class.forName("com.mysql.jdbc.Driver");
			stmt = conn.createStatement();

		} catch (ClassNotFoundException | SQLException exc) {
			exc.printStackTrace();
		}
	}
	
	public void createDatabase() {

		try {
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + db + ";");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ db
					+ ".airport(airport_id INT NOT NULL AUTO_INCREMENT, apt VARCHAR(3) UNIQUE NOT NULL, aptname Varchar(45) UNIQUE NOT NULL,PRIMARY KEY (airport_id));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ db
					+ ".company(company_id INT NOT NULL AUTO_INCREMENT, company VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (company_id));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ db
					+ ".route(route_id INT NOT NULL AUTO_INCREMENT, dep VARCHAR(45) NOT NULL, dest VARCHAR(45) NOT NULL, date VARCHAR(45) NOT NULL, company_id INT NOT NULL, PRIMARY KEY (route_id),FOREIGN KEY(company_id) REFERENCES company(company_id));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ db
					+ ".booking(booking_id INT NOT NULL AUTO_INCREMENT, firstname VARCHAR(45) NOT NULL, lastname VARCHAR(45) NOT NULL, pnr VARCHAR(45), route_id int NOT NULL, PRIMARY KEY (booking_id),FOREIGN KEY(route_id) REFERENCES route(route_id));");
			//predefine values in database
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".airport(apt,aptname) VALUES ('GBG','Gothenburg');");
			
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".airport(apt,aptname) VALUES ('KUL','Kualalumpur');");
			
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".airport(apt,aptname) VALUES ('ENG','Engelberg');");
			
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".company(company) VALUES ('Quantas');");
			
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".company(company) VALUES ('Lufthansa');");
			
			stmt.executeUpdate("INSERT IGNORE INTO "
					+db
					+ ".company(company) VALUES ('Aeroflot');");
			
			
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	public void setvaluesDb(String query) {
		try {
			stmt.executeUpdate(query);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public void getAirport() {

		try {
			ResultSet myRs = stmt.executeQuery("SELECT * FROM " + db
					+ ".airport ORDER BY airport_id ASC");
			System.out.println("Availiable airports.");
			while (myRs.next()) {

				int airpId = myRs.getInt("airport_id");
				String airP = myRs.getString("apt");
				String airpName= myRs.getString("aptname");	
				System.out.println(" " + airpId + ".\t" + airP + ".\t"+airpName+"\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public void getCompany() {

		try {
			ResultSet myRs = stmt.executeQuery("SELECT * FROM " + db
					+ ".company ORDER BY company_id ASC");
			System.out.println("Availiable companies.");
			while (myRs.next()) {

				int companyId = myRs.getInt("company_id");
				String company = myRs.getString("company");
				System.out.println(" " + companyId + ".\t" + company + "\t");
			}
			System.out.println("------------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public static void close() {

		try {

			conn.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	
	public String getDest(String dep){
		String result=null;
		
		try{
			ResultSet myRs= stmt.executeQuery("SELECT * FROM "+ db +".airport WHERE apt NOT LIKE '"+dep+"'");
			System.out.println();
			while (myRs.next()){
				int airpId = myRs.getInt("airport_id");
				String airP = myRs.getString("apt");
				String airpName= myRs.getString("aptname");	
				System.out.println(" " + airpId + ".\t" + airP + ".\t"+airpName+"\t");
			}
			System.out.println("---------------------");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return result;
	}
	public String getId(int id,String query){
		String value=null;
		
		try{
			ResultSet myRs= stmt.executeQuery(query);
			System.out.println();
			while(myRs.next()){
				value=myRs.getString(id);
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return value;
	}
	
	public void getRoute() {

		try {
			ResultSet myRs = stmt.executeQuery("SELECT route.route_id, route.dep, route.dest, route.date, company.company FROM " + db
					+ ".route JOIN "+db+".company on route.company_id=company.company_id ORDER BY route_id ASC");
			System.out.println("Availiable routes.");
			System.out.println("\tDeparture\tDestination\tDate\tCompany");
			while (myRs.next()) {

				int routeId = myRs.getInt("route_id");
				String dep = myRs.getString("dep");
				String dest = myRs.getString("dest");
				String date = myRs.getString("date");
				String company = myRs.getString("company");
				System.out.println(""+ routeId + ".\t" + dep + "\t\t" + dest + "\t\t" + date + "\t\t"+ company+ "");
			}
			System.out.println("------------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public void between(String departure, String destination){
		try{
			String query=null;
			query = "SELECT route.route_id, "
					+ "route.dep, "
					+ "route.dest, "
					+ "route.date, "
					+ "company.company "
					+ " FROM engelair.route JOIN engelair.company on route.company_id=company.company_id "
							+ " WHERE route.dep ='" + departure + "' AND route.dest='" + destination + "';";
			System.out.println(query);
			ResultSet myRs = stmt.executeQuery(query);
			while (myRs.next()){
				
				int routeId = myRs.getInt("route_id");
				String dep = myRs.getString("dep");
				String dest = myRs.getString("dest");
				String date = myRs.getString("date");
				String company = myRs.getString("company");
				System.out.println(" " + routeId + ".\t" + dep + "\t" + dest + "\t" + date + "\t"+ company+ "");
			}
			System.out.println("------------------------");
		}
		
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public void getBooking(String pnr){
		try{
			String query=null;
			query = "SELECT booking.booking_id, "
					+ "booking.firstname, "
					+ "booking.lastname, "
					+ "booking.pnr, "
					+ "route.dep, "
					+ "route.dest, "
					+ "route.date, "
					+ "company.company"
					+ "FROM engelair.booking "
					+ "JOIN engelair.route on booking.booking_id=route.route_id "
					+ "JOIN engelair.company on route.company_id=company.company_id "		
					+ "WHERE booking.pnr='" + pnr + "';";
			
			ResultSet myRs = stmt.executeQuery(query);
			while (myRs.next()){
				
				int booking_id = myRs.getInt("booking_id");
				String firstname = myRs.getString("firstname");
				String lastname = myRs.getString("lastname");
				String ssc = myRs.getString("pnr");
				String dep = myRs.getString("dep");
				String dest= myRs.getString("dest");
				String date= myRs.getString("date");
				String company= myRs.getString("company");
				
				System.out.println(" " + booking_id + ".\t" + firstname + "\t" + lastname + "\t" + ssc + "\t"+ dep+ "\t" + dest + "\t" + date + "\t" + company + "");
			}
			System.out.println("------------------------");
		}
		
		catch (Exception exc) {
			exc.printStackTrace();
		}
			
	}
	
	public void getAllBooking(){
		try{
			String query=null;
			query = "SELECT booking.booking_id, "
					+ "booking.firstname, "
					+ "booking.lastname, "
					+ "booking.pnr, "
					+ "route.dep, "
					+ "route.dest, "
					+ "route.date, "
					+ "company.company "
					+ "FROM engelair.booking "
					+ "JOIN engelair.route on booking.booking_id=route.route_id "
					+ "JOIN engelair.company on route.company_id=company.company_id ;";		
			System.out.println(query);		
			ResultSet myRs = stmt.executeQuery(query);
			while (myRs.next()){
				
				int booking_id = myRs.getInt("booking_id");
				String firstname = myRs.getString("firstname");
				String lastname = myRs.getString("lastname");
				String ssc = myRs.getString("pnr");
				String dep = myRs.getString("dep");
				String dest= myRs.getString("dest");
				String date= myRs.getString("date");
				String company= myRs.getString("company");
				
				System.out.println(" " + booking_id + ".\t" + firstname + "\t" + lastname + "\t" + ssc + "\t"+ dep+ "\t" + dest + "\t" + date + "\t" + company + "");
			}
			System.out.println("------------------------");
		}
		
		catch (Exception exc) {
			exc.printStackTrace();
		}
			
	}
	
	
}
