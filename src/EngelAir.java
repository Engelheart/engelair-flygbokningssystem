import java.util.Scanner;


public class EngelAir {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner user_input=new Scanner(System.in);
		String choice;
		Database db = new Database(); //creates object db
		db.createDatabase(); // method call that creates database if it doesn�t already exist
			
		System.out.println("What is your role in the system?");
		System.out.println("1. Are you an Administrator?");
		System.out.print("2. Are you a Traveller?");
		
		choice=user_input.next();
		if(choice.equals("1")||choice.equals("2")){
			if (choice.equals("1")){
				Admin myAdmin=new Admin();
				myAdmin.runAdmin();
				}
			if (choice.equals("2")){
				Traveller myTraveller=new Traveller();
				myTraveller.runTraveller();
			}
		}else{
			System.out.println("Invalid choice. Please try again");
		}
	}

}
